var getPriceXHRs = [];

function updateTicker() {
    console.log('updateTicker');
    total_value = 0;

    $.each(assets, function (index, value) {
        getPriceXHRs.push( getPrice(index, value));
    })

    $.when.apply(null, getPriceXHRs).then(function() {
        currencyObj = currencies.find(elem => elem.id == currency);
        currency_unit = currencyObj.unit;

        setAssetLengthClass(assets);

        $('#total_value').html("<h1>"+currency_unit+total_value+"</h1>");
    
        
        $('#ticker').html(coinTemplate({assets:assets, currency_symbol:currency_unit}));
    
        updateTitle(currency_unit, total_value);
    });
    




    updateProgressBar(progressBar);

}

function setAssetLengthClass(assets) {
    var assetsLengthPrevious = assetsLength;
    
    if (( assets.length % 5 == 0 && assets.length !== 0) || assets.length > 16) {
        assetsLength = "5";
    } else if ((assets.length % 4 == 0 && assets.length !== 0) || assets.length > 10) {
        assetsLength = "4";
    } else if (assets.length % 3 == 0 && assets.length !== 0) {
        assetsLength = "3";
    } else if (assets.length == 2) {
        assetsLength = "2";
    } else if (assets.length == 1) {
        assetsLength = "1";
    }
    
    $('#ticker').removeClass('coins-up-'+assetsLengthPrevious).addClass('coins-up-'+assetsLength);
}

function updateProgressBar(progressBar) {
    console.log('updateProgressBar');
    var curr = 0;
    progressBar.value = 0;
    clearInterval(update);
    update = setInterval(function() {
        progressBar.value = curr++;
    }, 60);
  }

function getCoinList(currency) {
    return $.ajax({
        url: 'https://api.coingecko.com/api/v3/coins/markets?vs_currency='+currency+'&order=market_cap_desc&per_page=250&page=1&sparkline=false',
        success: function(data) {
            localStorage.setItem('coinList', JSON.stringify(data));
        }
    });
}

function getPrice(index, asset) {
    return $.ajax({
        url: "https://api.coingecko.com/api/v3/coins/"+asset.id+"?localization=false&tickers=false&market_data=true&community_data=false&developer_data=false&sparkline=true",
        //async: false,
        success: function(data) {
            var market_data = data.market_data;
            var current_price = market_data.current_price[currency];
            var coin_image = data.image.small;
            var price_change_percentage_24h_in_currency = market_data.price_change_percentage_24h_in_currency.cad;
            assets[index]["current_price"] = current_price;
            assets[index]["asset_value"] = Math.round(current_price * asset.amount);
            assets[index]["price_change_percentage_24h_in_currency"] = (price_change_percentage_24h_in_currency) ? price_change_percentage_24h_in_currency.toFixed(2) : "";
            assets[index]["image"] = coin_image;
            total_value += assets[index]["asset_value"];
            assets[index].positive_change = (assets[index]["price_change_percentage_24h_in_currency"] > 0) ? true : false;
        }
    });
}
        
function updateTitle(currency_unit, total_value) {
    $('head title').text(currency_unit+numeral(total_value).format('0.00a')+' Crypto Assets');
}

function renderCoinLists(assets, coin_list) {
    if (!$.isEmptyObject(assets)) {
        var obj;
        $.each(assets, function(index, value) {
            obj = coin_list.find(elem => elem.id == value.id);
            obj.selected=true;
            $('#add_tokens tbody').append(rowTemplate({coinList:coin_list, amount:value.amount}));
            coinSelect = new Choices($("select.coin-select")[index], {
                itemSelectText: '',
				position: "bottom"
            });
            obj.selected=false;
        });
        
        localStorage.setItem('coinList', JSON.stringify(coin_list));

    } else {
        for (var i = 0; i<4; i++) {
            $('#add_tokens tbody').append(rowTemplate({coinList:coin_list}));
            coinSelect = new Choices($("select.coin-select")[i], {
                itemSelectText: '',
                position: "bottom"
            });
        }
    }
}
